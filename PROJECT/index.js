console.log ("Hi from index.js");
var debug_gl;

document.getElementById("loadFileButton").addEventListener("click",() => {
    console.log("btn clicked");
    var fileInput = document.getElementById("fileInput");

    Papa.parse(fileInput.files[0], {
        preview: 1,
        complete: function(results) {
            //console.log(results.data);
            populate_csvHeadingsSelect(results.data[0]);
        }
    });
});

function populate_csvHeadingsSelect(headingData) {
    // expects the heading row data from the parsed csv i.e. results.data[0]
    let select = document.getElementById("csvHeadingsSelect")
    for (i in headingData) {
        //console.log(i + "," + headingData[i])
        // populate select
        var opt = document.createElement('option');
        opt.value = i;
        opt.innerHTML = headingData[i];
        select.appendChild(opt);
        console.log("Loaded CSV Headings")
    }
};

function search(searchStr) {
    
}
